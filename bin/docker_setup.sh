#!/bin/sh

cp config/dev.secret.exs.example.docker config/dev.secret.exs
cp config/test.secret.exs.example.docker config/test.secret.exs

mix deps.get
mix deps.compile
(cd apps/db && mix ecto.setup)

(cd apps/admin_web/assets && npm install && node node_modules/webpack/bin/webpack.js --mode development)
(cd apps/xbins_web/assets && npm install && node node_modules/webpack/bin/webpack.js --mode development)
