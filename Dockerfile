FROM elixir:1.10.4-alpine as build
COPY --from=node:14.5.0-alpine / /

# install build dependencies
RUN apk add --update build-base git

# prepare build dir
RUN mkdir /app
WORKDIR /app

# install hex + rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# build-time vars
ARG DATABASE_URL
ARG SECRET_KEY_BASE
ARG MIX_ENV

# set build ENV
ENV DATABASE_URL ${DATABASE_URL}
ENV SECRET_KEY_BASE ${SECRET_KEY_BASE}
ENV MIX_ENV ${MIX_ENV}

# umbrella
COPY mix.exs mix.lock ./
COPY config config

# apps
COPY apps apps
RUN mix deps.get

# build assets in apps
WORKDIR /app/apps/admin_web
RUN cd assets && npm install && npm run deploy
RUN mix phx.digest

WORKDIR /app/apps/xbins_web
RUN cd assets && npm install && npm run deploy
RUN mix phx.digest

RUN mix deps.compile

# build project
WORKDIR /app
#COPY rel rel
RUN mix compile
RUN MIX_ENV=${MIX_ENV} mix release

# prepare release image
FROM alpine:3.12.0 AS app

RUN apk add --update bash openssl imagemagick

RUN mkdir /app
WORKDIR /app

ARG MIX_ENV

ENV MIX_ENV ${MIX_ENV}

COPY --from=build /app/_build/${MIX_ENV}/rel/xbins_umbrella ./
RUN chown -R nobody: /app
USER nobody

ENV HOME=/app

CMD ["sh","-c","bin/xbins_umbrella eval Db.Release.migrate && bin/xbins_umbrella start"]
