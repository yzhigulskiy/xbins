# xbins
Better Interviews (New Version)

## Install via docker

- Install [Docker](https://docs.docker.com/install/)
- Run app:
  - `docker-compose build`
  - `docker-compose run --rm web bin/docker_setup.sh`
  - `docker-compose up`
- Run tests:
  - `docker-compose run --rm web mix test`

## Install on local machine

- Install [ASDF](https://github.com/asdf-vm/asdf).
- Install [Erlang](https://github.com/asdf-vm/asdf-erlang) ASDF plugin
  - Install Erlang [Requirements](https://github.com/asdf-vm/asdf-erlang#before-asdf-install)
  - `asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git`
  - `export KERL_CONFIGURE_OPTIONS="--disable-debug --without-javac"`
- Install [Elixir](https://github.com/asdf-vm/asdf-elixir) ASDF plugin
  - `asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git`
- Install [Nodejs](https://github.com/asdf-vm/asdf-nodejs) ASDF plugin
  - Install Nodejs [Requirements](https://github.com/asdf-vm/asdf-nodejs#requirements)
  - `asdf plugin-add nodejs https://github.com/asdf-vm/asdf-nodejs.git`
  - `bash ~/.asdf/plugins/nodejs/bin/import-release-team-keyring`
- CD into project folder
- Install Erlang, Elixir, NodeJs:
  - `asdf install`
- Reopen your shell.
- Copy example configs:
  - `cp config/dev.secret.exs.example config/dev.secret.exs`
  - `cp config/test.secret.exs.example config/test.secret.exs`
- Install Phoenix 1.5.3
  - `mix archive.install hex phx_new 1.5.3`
- Configure DB parameters in
  - `config/dev.secret.exs`
  - `config/test.secret.exs`
- Run:
  - `mix deps.get`
  - `cd apps/admin_web/assets && npm install && node node_modules/webpack/bin/webpack.js --mode development && cd ../../..`
  - `cd apps/xbins_web/assets && npm install && node node_modules/webpack/bin/webpack.js --mode development && cd ../../..`
  - `mix deps.compile`
  - `cd apps/db && mix ecto.setup && cd ../..`
- Run the server:
  - `iex -S mix phx.server`
- Go to:
  - admin panel: `http://localhost:4001/`
  - client panel: `http://localhost:4000/`

## Decrypt values.yaml files
- Install helm secret plugin
  ```helm plugin install https://github.com/zendesk/helm-secrets```
- Create and download json secret key for service account "helm-ecnrypter-decrypter"
  GCP -> IAM & Admin -> Service Accounts -> helm-ecnrypter-decrypter -> Keys -> ADD KEY -> Create new key -> Key type (JSON) -> Create
- ```export GOOGLE_APPLICATION_CREDENTIALS=~/Downloads/xbins-better-interviews-330a59f070ad.json```
- ```helm secrets dec values-stage.yaml```
- ```helm upgrade --install --debug --dry-run xbins xbins -f xbins/values-stage.yaml.dec```

## Janus

Demo video chat: [/video_chat](http://localhost:4000/video_chat)
recordings page: [/recordings](http://localhost:4000/recordings)

### External sources

[WebRTC server](https://janus.conf.meetecho.com/)

[NPM gateway](http://github.com/meetecho/janus-gateway.git)

[Docker gateway](https://hub.docker.com/r/canyan/janus-gateway)

### Ports

- `:80` expose Janus documentation and admin/monitoring website
- `:7088` expose Admin/monitor server
- `:8088` expose Janus server
- `:8188` expose Websocket server
- `:10000-10200/udp` used during session establishment

### Recordings

All records have specific format: `{description}-{room_id}-{role}-{timestamp}-{video|audio}.mjr`
and lives in [janus/recordings](janus/recordings) folder

To use GStorage locally:
- Create `google_credentials.json` and put credentials
- Inside [dev.exs](config/dev.exs) replace line `config :goth, disabled: true`
to `config :goth, json: Path.join(__DIR__, "../google_credentials.json") |> File.read!()`

#### Convert from .mjr to .mp4

- Make sure videocodec is `h264`
- Run script: `docker-compose exec janus-gateway mjr2mp4`
- Inside [janus/recordings](janus/recordings) will be created `{description}-{room_id}-{role}-{timestamp}.mp4` file(s)

#### Convert from .mjr to .webm (deprecated)

- Make sure videocodec is `vp8`
- Run script: `docker-compose exec janus-gateway mjr2webm`
- Inside [janus/recordings](janus/recordings) will be created `{description}-{room_id}-{role}-{timestamp}.webm` file(s)

#### Move recordings to storage

- Run: `docker-compose exec janus-gateway move2storage`
