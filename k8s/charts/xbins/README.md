# Helm Chart for App Stack

## TL;DR;

```console
$ helm install .
```

## Introduction

This chart bootstraps an App Stack deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

This chart has been tested to work with NGINX Ingress, cert-manager, fluentd and Prometheus.
Currently support only Ruby on Rails.

## Prerequisites

- Kubernetes 1.12+
- PV provisioner support in the underlying infrastructure
- other

## Chart internals in details.

### Secrets

All environment variables for application stored in kubernetes [Secrets](https://kubernetes.io/docs/concepts/configuration/secret/).
For example, during initial deployment, variables for backend provided in values file:

```
envApp:
  RAILS_MASTER_KEY: c3a142f112c872fed7b52986eaf3c7b4
  DB_NAME: testimonials_production
  DB_USERNAME: testimonials
  DB_PASSWORD: vYVHe72GtA39dbvUNFk2qA9sg
```

stored and available in related secret in cluster:

```
$ kubectl get secret dev-app-stack-rails-secret -o yaml
apiVersion: v1
data:
  DB_NAME: dGVzdGltb25pYWxzX3Byb2R1Y3Rpb24=
  DB_PASSWORD: dllWSGU3Mkd0QTM5ZGJ2VU5GazJxQTlzZw==
  DB_USERNAME: dGVzdGltb25pYWxz
  RAILS_LOG_TO_STDOUT: MQ==
  RAILS_MASTER_KEY: YzNhMTQyZjExMmM4NzJmZWQ3YjUyOTg2ZWFmM2M3YjQ=
kind: Secret
...
```

The data field is used to store arbitrary data, encoded using base64.
So later if you need to store another variable(s) in a Secret using the data field, convert them to base64 as follows:

```
$ echo -n 'SuperS3cr3t_Password' | base64
U3VwZXJTM2NyM3RfUGFzc3dvcmQ=
```

then add to secret with edit:

```
$ kubectl edit secret dev-app-stack-rails-secret
apiVersion: v1
data:
  DB_NAME: dGVzdGltb25pYWxzX3Byb2R1Y3Rpb24=
  DB_PASSWORD: dllWSGU3Mkd0QTM5ZGJ2VU5GazJxQTlzZw==
  DB_USERNAME: dGVzdGltb25pYWxz
  RAILS_LOG_TO_STDOUT: MQ==
  RAILS_MASTER_KEY: YzNhMTQyZjExMmM4NzJmZWQ3YjUyOTg2ZWFmM2M3YjQ=
  SUPER_SECRET_PASSWORD: U3VwZXJTM2NyM3RfUGFzc3dvcmQ=
kind: Secret
...
```

**NOTE:** keep in mind, that after adding new variables in Secret you must restart/recreate associated pod(s):

```
# via kubectl
$ kubectl delete po dev-app-stack-rails-bb4bfd89-n7vx5

# via helm - recreate pods for whole stack
helm upgrade --install dev-app-stack . --namespace dev --recreate-pods
```

### Front-end

Part of deployment, which is in charge of front-end part of application.
Configuration settings are available in related [values.yaml](./values.yaml) section:

```
...
frontend:
  enabled: true
  name: react
  replicaCount: 1
  image:
    repository: gcr.io/testimonials-project/nginx
    tag: 0.2
...  
```

Usually it is Nginx proxy service, which serve static assets and proxying API requests to back-end.
Here [nginx.conf](../../../examples/nginx.conf) example for JS and RoR and [Dockerfile.nginx](../../../examples/Dockerfile.nginx) to build image.

### Back-end

Part of deployment, which is in charge of back-end part of application.
Configuration settings are available in related [values.yaml](./values.yaml) section:

```
...
backend:
  enabled: true
  name: rails
  replicaCount: 1
  image:
    repository: gcr.io/testimonials-project/app
    tag: 0.2
...  
```

Here base [Dockerfile](../../../examples/Dockerfile) to build image.
Depending on your application, you may want to change type of application server which will run in container, e.g. in current [implementation](./templates/back-deployment.yaml) we start Puma:

```
...
          command: ["bundle", "exec", "rails", "s", "-e", "$(RAILS_ENV)", "-p", "{{ .Values.backend.service.port }}"]
...
```

This command just override default [entrypoint.sh](../../../examples/entrypoint.sh) script for Docker image.

If you are using background processing for Ruby, you may want to enable Sidekiq, by changing related value:

```
...
sidekiq:
  enabled: true
  replicaCount: 1
...
```

**NOTE:** Keep in mind, that Sidekiq utilize Redis, so it should be enabled too (described below).

```
...
redis:
  enabled: true
...
```


### Databases

As an option, you can enable PostgreSQL and Redis databases support in chart. In that case yo need to enable them by changing related values in [values.yaml](./values.yaml) file:

```
postgresql:
  enabled: true
...
redis:
  enabled: true
...

```

**NOTE:** specify the same database credentials for both PostgreSQL and back-end. Database setting for back-end:

```
...
backend:
...
  envApp:
    RAILS_MASTER_KEY: c3a142f112c872fed7b52986eaf3c7b4
    DB_NAME: testimonials_production
    DB_USERNAME: testimonials
    DB_PASSWORD: vYVHe72GtA39dbvUNFk2qA9sg
...
```

Usually this step should be done once before initial deployment as databases deployments still unchanged in further application life-cycle.

If you do not want to deploy database services inside k8s cluster - just disable services and specify credentials in environment variables as described above.

### Ingress

Required for domain/path-based routing and SSL termination automation. General setup and usage described in [README.md](../../README.md)


## Installing the Chart

* To install the chart with the release name `my-release`:

```
$ helm upgrade --install my-release .
```

The command deploys Stack on the Kubernetes cluster in the default configuration.

> **Tip**: List all releases using `helm list`

* To deploy stack in separate environment, e.g. staging ([namespace](../../README.md) must be existed in cluster), just provide `--namespace` flag:

```
$ helm install --name my-release . -f values-staging.yaml --namespace dev
```

* If Postgresql or(and) Redis support option enabled, update dependencies before install:

```
$ helm dependency update
Hang tight while we grab the latest from your chart repositories...
...
Update Complete. ⎈Happy Helming!⎈
Saving 2 charts
Downloading postgresql from repo https://kubernetes-charts.storage.googleapis.com
Downloading redis from repo https://kubernetes-charts.storage.googleapis.com
Deleting outdated charts

$ helm dependency list
NAME      	VERSION	REPOSITORY                                      	STATUS
postgresql	5.3.12 	https://kubernetes-charts.storage.googleapis.com	ok
redis     	8.1.3  	https://kubernetes-charts.storage.googleapis.com	ok
```

* Verify deployment without installation:

```
helm upgrade --install dev-app-stack . --namespace dev --debug --dry-run
```

* Check release deployment status:

```
$ helm status dev-app-stack
LAST DEPLOYED: Wed Jul 17 10:53:55 2019
NAMESPACE: dev
STATUS: DEPLOYED

RESOURCES:
==> v1/ConfigMap
NAME                        DATA  AGE
dev-app-stack-redis         3     98s
dev-app-stack-redis-health  6     98s

==> v1/Deployment
NAME                 READY  UP-TO-DATE  AVAILABLE  AGE
dev-app-stack-rails  1/1    1           1          12h
dev-app-stack-react  1/1    1           1          12h

==> v1/Pod(related)
NAME                                  READY  STATUS   RESTARTS  AGE
dev-app-stack-postgresql-0            1/1    Running  0         94s
dev-app-stack-rails-549dbb77fb-dzrkv  1/1    Running  0         94s
dev-app-stack-react-7ff8ccd5cd-mjx9s  1/1    Running  0         12h
dev-app-stack-redis-master-0          1/1    Running  0         94s

==> v1/Secret
NAME                        TYPE    DATA  AGE
dev-app-stack-postgresql    Opaque  1     98s
dev-app-stack-rails-secret  Opaque  4     12h
dev-app-stack-redis         Opaque  1     98s

==> v1/Service
NAME                               TYPE       CLUSTER-IP   EXTERNAL-IP  PORT(S)   AGE
dev-app-stack-postgresql           ClusterIP  10.0.4.202   <none>       5432/TCP  97s
dev-app-stack-postgresql-headless  ClusterIP  None         <none>       5432/TCP  97s
dev-app-stack-rails                ClusterIP  10.0.10.228  <none>       3000/TCP  12h
dev-app-stack-react                ClusterIP  10.0.8.202   <none>       80/TCP    12h
dev-app-stack-redis-headless       ClusterIP  None         <none>       6379/TCP  96s
dev-app-stack-redis-master         ClusterIP  10.0.4.216   <none>       6379/TCP  96s

==> v1/ServiceAccount
NAME                 SECRETS  AGE
dev-app-stack-redis  1        97s

==> v1beta1/Ingress
NAME                   HOSTS       ADDRESS         PORTS    AGE
dev-app-stack-ingress  govale.org  35.226.249.157  80, 443  12h

==> v1beta1/RoleBinding
NAME                 AGE
dev-app-stack-redis  97s

==> v1beta2/StatefulSet
NAME                        READY  AGE
dev-app-stack-postgresql    1/1    95s
dev-app-stack-redis-master  1/1    95s


NOTES:
1. Get the application URL by running these commands:
  https://govale.org/
```

## Configuration

Configurable parameters of the App chart and their default values provided in [values.yaml](./values.yaml).

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```
$ helm install --name my-release . \
  --set redis.enabled=false,frontend.image.tag=0.3
```

The above command sets the frontend to use image with tag 0.3. Additionally it enables Redis.

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```
$ helm install --name my-release . -f values.yaml
```

The usual approach is to copy default values file, update required values then apply during install/upgrade:

```
$ cp values.yaml values-staging.yaml

$ vi values-staging.yaml # update some settings, e.g. credentials

$ helm install --name my-release . -f values-staging.yaml
```

Typically it is required once, basically we will change only image tag for particular service. And mostly via CI/CD tools.

To get a current values file for a given release:

```
$ helm get values -a dev-app-stack > dev-app-stack.yaml
```


## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```
$ helm delete --purge my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.
