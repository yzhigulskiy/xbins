#!/bin/sh
set -e

GC_CREDS="/usr/local/etc/janus/creds.json"
RECORDING_BUCKET="xbins-recording"
FILES=$(find 'recordings' -name '*.mp4')
FAILED=""

if [ ! -x "$(command -v gcloud)" ] || [ ! -x "$(command -v gsutil)" ]; then
  printf "\033[31mERROR\033[0m This script requires \033[1mgoogle-cloud-sdk\033[0m\n"
  exit 1
fi

if gcloud auth list 2>&1 | grep -q "No credentialed accounts"; then
    if [ -f "$GC_CREDS" ]; then
      gcloud auth activate-service-account --key-file $GC_CREDS
    else
      printf "\033[31mERROR\033[0m gcloud credentials file not found: \033[1m$GC_CREDS\033[0m\n"
      exit 1
    fi
fi

set +e
for file in $FILES
do
  printf "  -> \033[33mUploading to storage\033[0m\n"
  gsutil mv $file gs://$RECORDING_BUCKET >/dev/null 2>&1
  RESULT=$?
  if [ $RESULT -eq 0 ]; then
    printf "  -> \033[32mCompleted\033[0m\n"
  else
    FAILED="$FAILED "$file
    printf "  -> \033[31mFailed\033[0m\n"
  fi
done
printf "\033[36mFinished processing\033[0m\n"
if [ -n "$FAILED" ]; then
  printf "\n\033[31mFailed uploading following recordings\033[0m\n"
  for file in $FAILED; do
    printf "  $file\n"
  done
fi
