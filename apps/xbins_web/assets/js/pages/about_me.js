import Croppie from 'croppie';

(function(){
  var fileInput = document.getElementById('avatar-input');
  var el = document.getElementById('croppie-avatar');
  var applyAvatarChanges = document.getElementById('apply-avatar-changes');

  var image = document.getElementById('avatar-image');
  var label = document.getElementById('user-avatar-label');
  var avatarSection = document.getElementById('croppie-avatar-section');

  var preview = new Croppie(el, {
    enableExif: false,
    boundary: { width: 250, height: 250 },
    viewport: { width: 200, height: 200 }
  });

  function readFile(input) {
    if (input.files && input.files[0]) {
      label.classList.add("hidden");
      avatarSection.classList.remove("hidden");
      var reader = new FileReader();
      reader.onload = function (e) {
        preview.bind({url: e.target.result})
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  fileInput.addEventListener("change", function () { readFile(this) });

  applyAvatarChanges.addEventListener('click', function (ev) {

    preview.result({
      type: 'base64',
      format : 'png',
      size: 'viewport'
    }).then(function (blob) {
      var object = {avatar: blob};
      let json = JSON.stringify(object);

      var request = new XMLHttpRequest();
      request.open("PUT", "/api/about_me");
      request.setRequestHeader('Content-type', 'application/json; charset=utf-8');

      request.send(json);

      image.setAttribute('src', blob);
      label.classList.remove("hidden");
      avatarSection.classList.add("hidden");

    });
  });

})();
