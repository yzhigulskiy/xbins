defmodule XbinsWeb.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      XbinsWeb.Telemetry,
      # Start the Endpoint (http/https)
      XbinsWeb.Endpoint,
      # Start the PubSub system
      {Phoenix.PubSub, name: XbinsWeb.PubSub}
      # Start a worker by calling: XbinsWeb.Worker.start_link(arg)
      # {XbinsWeb.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: XbinsWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    XbinsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
