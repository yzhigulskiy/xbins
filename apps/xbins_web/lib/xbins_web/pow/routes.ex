defmodule XbinsWeb.Pow.Routes do
  use Pow.Phoenix.Routes

  alias XbinsWeb.Router.Helpers, as: Routes

  def after_sign_in_path(conn), do: Routes.about_me_path(conn, :edit)

  def after_registration_path(conn), do: Routes.about_me_path(conn, :edit)
end
