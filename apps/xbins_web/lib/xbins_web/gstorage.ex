defmodule XbinsWeb.GStorage do
  alias GoogleApi.Storage.V1.Api.Objects
  alias GoogleApi.Storage.V1.Connection

  @bucket Application.get_env(:xbins_web, :janus)[:storage_bucket]
  @scope "https://www.googleapis.com/auth/cloud-platform"

  def conn do
    Connection.new(get_token())
  end

  defp get_token do
    {:ok, token} = Goth.Token.for_scope(@scope)
    token.token
  end

  def get_objects_path do
    {:ok, response} = Objects.storage_objects_list(conn(), @bucket)

    for item <- response.items do
      resource = Guss.new(@bucket, item.name)
      # resource url expires in 1 hour
      {:ok, response} = Guss.sign(resource)
      response
    end
  rescue
    _e -> Path.wildcard("janus/recordings/*.mp4")
  end
end
