defmodule XbinsWeb.Router do
  use XbinsWeb, :router
  use Pow.Phoenix.Router

  use Pow.Extension.Phoenix.Router,
    extensions: [PowResetPassword]

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]

    resources "/api/about_me", XbinsWeb.Api.AboutMeController, only: [:update], singleton: true
  end

  scope "/", XbinsWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/video_chat", VideoChatController, :index
    get "/recordings", VideoChatController, :list
    get "/learner_package_quiz/:step_name", LearnerPackageQuizController, :edit
    put "/learner_package_quiz", LearnerPackageQuizController, :update
    resources "/about_me", AboutMeController, only: [:edit, :update], singleton: true
  end

  # Other scopes may use custom stacks.
  # scope "/api", XbinsWeb do
  #   pipe_through :api
  # end

  pipeline :pow_layout do
    plug :put_layout, {XbinsWeb.LayoutView, "pow.html"}
  end

  scope "/" do
    pipe_through [:browser, :pow_layout]

    pow_routes()
    pow_extension_routes()

    get "/reset_password/note", XbinsWeb.ResetPasswordController, :note
    post "/reset_password", XbinsWeb.ResetPasswordController, :create
  end
end
