defmodule XbinsWeb.AboutMeView do
  use XbinsWeb, :view

  alias FileStorage.Uploaders.Avatar

  def user_avatar(user) do
    Avatar.url({"file.png", user})
  end
end
