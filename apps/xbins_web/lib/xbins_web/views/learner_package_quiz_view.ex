defmodule XbinsWeb.LearnerPackageQuizView do
  use XbinsWeb, :view

  def video_call?(learner_package_quiz) do
    Regex.match?(~r/video/i, learner_package_quiz.communication_channel)
  end
end
