defmodule XbinsWeb.VideoChatController do
  use XbinsWeb, :controller

  def index(conn, _params) do
    rooms = XbinsWeb.Janus.list_rooms()

    room_id =
      if length(rooms) > 0 do
        List.first(rooms)["room"]
      else
        XbinsWeb.Janus.create_room("room")
      end

    user = Pow.Plug.current_user(conn)
    role = if user, do: user.id, else: 0

    render(
      conn,
      "index.html",
      room_id: room_id,
      role: role
    )
  end

  def list(conn, _params) do
    render(
      conn,
      "recordings.html",
      recordings: XbinsWeb.GStorage.get_objects_path()
    )
  end
end
