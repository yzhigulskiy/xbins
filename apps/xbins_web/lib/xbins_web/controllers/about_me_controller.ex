defmodule XbinsWeb.AboutMeController do
  use XbinsWeb, :controller
  alias Db.User

  plug :put_layout, "learner.html"

  def edit(conn, _params) do
    current_user = Pow.Plug.current_user(conn)
    changeset = User.change_candidate(current_user)

    render(
      conn,
      "edit.html",
      changeset: changeset,
      user: current_user
    )
  end

  def update(conn, %{"user" => params}) do
    current_user = Pow.Plug.current_user(conn)
    changeset = User.change_candidate(current_user)

    case User.update_candidate(current_user, params) do
      {:ok, _user} ->
        conn
        |> redirect(to: Routes.about_me_path(conn, :edit))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(
          conn,
          "edit.html",
          changeset: changeset,
          user: current_user
        )
    end
  end
end
