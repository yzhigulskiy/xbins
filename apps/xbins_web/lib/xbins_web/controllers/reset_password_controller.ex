defmodule XbinsWeb.ResetPasswordController do
  use XbinsWeb, :controller
  alias PowResetPassword.{Phoenix.Mailer, Plug}

  def create(conn, %{"user" => user_params}) do
    {status, changeset, conn} = Plug.create_reset_token(conn, user_params)

    if status == :ok do
      url = Routes.pow_reset_password_reset_password_url(conn, :edit, changeset.token)
      deliver_email(conn, changeset.user, url)
    end

    conn |> redirect(to: Routes.reset_password_path(conn, :note))
  end

  def note(conn, _params) do
    render(conn, "note.html")
  end

  defp deliver_email(conn, user, url) do
    email = Mailer.reset_password(conn, user, url)
    Pow.Phoenix.Mailer.deliver(conn, email)
  end
end
