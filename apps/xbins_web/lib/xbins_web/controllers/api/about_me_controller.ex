defmodule XbinsWeb.Api.AboutMeController do
  use XbinsWeb, :controller
  alias Db.User

  def update(conn, params) do
    current_user = Pow.Plug.current_user(conn)
    changeset = User.change_candidate(current_user)

    encoded_params = %{
      avatar: %{
        filename: "some.png",
        binary:
          params["avatar"] |> String.replace(~r/^data\:image\/.*;base64,/, "") |> Base.decode64!()
      }
    }

    case User.update_avatar(current_user, encoded_params) do
      {:ok, user} ->
        conn
        |> pretty_json(%{ok: true})

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> pretty_json(%{ok: false})
    end
  end

  def pretty_json(conn, data) do
    conn
    |> Plug.Conn.put_resp_header("content-type", "application/json; charset=utf-8")
    |> Plug.Conn.send_resp(200, Poison.encode!(data, pretty: true))
  end
end
