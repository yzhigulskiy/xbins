defmodule XbinsWeb.LearnerPackageQuizController do
  use XbinsWeb, :controller

  alias Db.LearnerPackage

  plug :maybe_assign_quiz_token

  def edit(conn, _params = %{"step_name" => step_name}) do
    quiz_token = quiz_token(conn)
    learner_package_quiz = LearnerPackage.find_or_create_quiz!(quiz_token)
    changeset = LearnerPackage.change_learner_package_quiz(learner_package_quiz)

    render(
      conn,
      "edit.html",
      learner_package_quiz: learner_package_quiz,
      changeset: changeset,
      step_name: step_name
    )
  end

  def update(conn, %{"learner_package_quiz" => params}) do
    quiz_token = Plug.Conn.get_session(conn, :quiz_token)
    learner_package_quiz = LearnerPackage.find_or_create_quiz!(quiz_token)

    case LearnerPackage.update_learner_package_quiz(learner_package_quiz, params) do
      {:ok, learner_package_quiz} ->
        conn
        |> redirect(
          to: Routes.learner_package_quiz_path(conn, :edit, step_name_for(learner_package_quiz))
        )

      {:error, %Ecto.Changeset{} = changeset} ->
        render(
          conn,
          "edit.html",
          learner_package_quiz: learner_package_quiz,
          changeset: changeset,
          step_name: learner_package_quiz.step_name
        )
    end
  end

  def quiz_token(conn) do
    Plug.Conn.get_session(conn, :quiz_token)
  end

  def maybe_assign_quiz_token(conn, _opts) do
    quiz_token = Plug.Conn.get_session(conn, :quiz_token)

    if quiz_token do
      conn
    else
      token = create_token()
      conn = Plug.Conn.put_session(conn, :quiz_token, token)

      conn
    end
  end

  defp create_token do
    UUID.uuid4()
  end

  defp step_name_for(learner_package_quiz) do
    LearnerPackage.step_name_for(learner_package_quiz)
  end
end
