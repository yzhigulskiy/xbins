defmodule XbinsWeb.Janus do
  @server_url Application.get_env(:xbins_web, :janus)[:url]
  @plugin "janus.plugin.videoroom"

  def create_room(description) do
    HTTPoison.start()
    transaction = create_transaction()
    session = create_janus_session(transaction)
    plugin_session = create_plugin_session(transaction, session)

    body =
      Poison.encode!(%{
        janus: "message",
        transaction: transaction,
        body: %{
          request: "create",
          description: description,
          record: true,
          videocodec: "h264",
          publishers: 2,
          rec_dir: "/recordings"
        }
      })

    response = make_post("#{@server_url}/#{session}/#{plugin_session}", body)

    if response != nil do
      response["plugindata"]["data"]["room"]
    end
  end

  def list_rooms do
    HTTPoison.start()
    transaction = create_transaction()
    session = create_janus_session(transaction)
    plugin_session = create_plugin_session(transaction, session)

    body =
      Poison.encode!(%{
        janus: "message",
        transaction: transaction,
        body: %{
          request: "list"
        }
      })

    response = make_post("#{@server_url}/#{session}/#{plugin_session}", body)

    if response != nil do
      response["plugindata"]["data"]["list"]
    end
  end

  defp create_janus_session(transaction) do
    body = Poison.encode!(%{janus: "create", transaction: transaction})
    response = make_post(@server_url, body)

    if response != nil do
      response["data"]["id"]
    end
  end

  defp create_plugin_session(transaction, session) do
    body = Poison.encode!(%{janus: "attach", plugin: @plugin, transaction: transaction})
    response = make_post("#{@server_url}/#{session}", body)

    if response != nil do
      response["data"]["id"]
    end
  end

  defp make_post(url, body) do
    case HTTPoison.post(url, body) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        Poison.decode!(body)

      {:ok, %HTTPoison.Response{body: _body}} ->
        nil

      {:error, %HTTPoison.Error{reason: _reason}} ->
        nil
    end
  end

  defp create_transaction do
    UUID.uuid1()
  end
end
