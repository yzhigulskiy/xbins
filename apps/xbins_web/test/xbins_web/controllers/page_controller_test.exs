defmodule XbinsWeb.PageControllerTest do
  use XbinsWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Sign in"
  end
end
