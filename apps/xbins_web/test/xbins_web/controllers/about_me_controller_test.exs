defmodule XbinsWeb.AboutMeControllerTest do
  use XbinsWeb.ConnCase

  setup do
    user = Factory.insert!(:user, first_name: "TestFirstName")

    {:ok, user: user}
  end

  test "GET /about_me/edit", %{conn: conn, user: user} do
    conn = conn |> authenticate(user) |> get("/about_me/edit")
    assert html_response(conn, 200) =~ "TestFirstName"
  end
end
