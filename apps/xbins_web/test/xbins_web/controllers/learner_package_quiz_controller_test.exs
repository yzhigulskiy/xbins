defmodule XbinsWeb.LearnerPackageQuizControllerTest do
  use XbinsWeb.ConnCase

  alias Db.LearnerPackage

  @create_attrs %{
    "first_name" => "some first_name",
    "token" => "#{System.unique_integer()}-test"
  }
  @invalid_attrs %{"step_name" => "email"}

  def fixture(:learner_package_quiz) do
    {:ok, learner_package_quiz} = LearnerPackage.create_learner_package_quiz(@create_attrs)
    learner_package_quiz
  end

  describe "edit learner_package_quiz" do
    setup [:create_learner_package_quiz]

    test "renders form for editing chosen learner_package_quiz", %{
      conn: conn,
      learner_package_quiz: _learner_package_quiz
    } do
      conn = get(conn, Routes.learner_package_quiz_path(conn, :edit, "name"))
      assert html_response(conn, 200) =~ "Let's get to know each other"
    end
  end

  describe "update learner_package_quiz" do
    setup [:create_learner_package_quiz]

    test "renders errors when data is invalid", %{
      conn: conn,
      learner_package_quiz: learner_package_quiz
    } do
      conn =
        conn
        |> Plug.Test.init_test_session(quiz_token: learner_package_quiz.token)
        |> put(Routes.learner_package_quiz_path(conn, :update),
          learner_package_quiz: @invalid_attrs
        )

      assert html_response(conn, 200) =~ "Let's get to know each other"
    end
  end

  defp create_learner_package_quiz(_) do
    learner_package_quiz = fixture(:learner_package_quiz)
    %{learner_package_quiz: learner_package_quiz}
  end
end
