defmodule XbinsWeb.GStorageTest do
  use XbinsWeb.ConnCase

  import Mock

  describe "get_objects_path" do
    test "storage objects" do
      with_mocks([
        {Goth.Token, [], goth_mock()},
        {GoogleApi.Storage.V1.Connection, [], connection_mock()},
        {GoogleApi.Storage.V1.Api.Objects, [], gstorage_mock()},
        {Guss, [], guss_mock()}
      ]) do
        assert ["test_cloud_path"] == XbinsWeb.GStorage.get_objects_path()
      end
    end

    test "local objects" do
      with_mocks([{Path, [], path_mock()}]) do
        assert ["test_local_path"] == XbinsWeb.GStorage.get_objects_path()
      end
    end
  end

  defp connection_mock do
    [new: fn _token -> "" end]
  end

  defp goth_mock do
    [for_scope: fn _scope -> {:ok, %{:token => ""}} end]
  end

  defp gstorage_mock do
    [storage_objects_list: fn _conn, _bucket -> {:ok, %{:items => [%{:name => ""}]}} end]
  end

  defp guss_mock do
    [new: fn _conn, _bucket -> "" end, sign: fn _resource -> {:ok, "test_cloud_path"} end]
  end

  defp path_mock do
    [wildcard: fn _path -> ["test_local_path"] end]
  end
end
