defmodule XbinsWeb.JanusTest do
  use XbinsWeb.ConnCase

  import Mock

  describe "create_room" do
    test "success" do
      body =
        Poison.encode!(%{
          plugindata: %{data: %{room: 1}},
          data: %{id: 1}
        })

      with_mock HTTPoison, janus_mock(:ok, body, 200) do
        assert 1 == XbinsWeb.Janus.create_room("description")
      end
    end

    test "400 response" do
      body = Poison.encode!(%{})

      with_mock HTTPoison, janus_mock(:ok, body, 400) do
        assert is_nil(XbinsWeb.Janus.create_room("description"))
      end
    end

    test "error response" do
      body = Poison.encode!(%{})

      with_mock HTTPoison, janus_mock(:error, body, 500) do
        assert is_nil(XbinsWeb.Janus.create_room("description"))
      end
    end
  end

  describe "list_rooms" do
    test "success" do
      body =
        Poison.encode!(%{
          plugindata: %{data: %{list: [%{room: 1}]}},
          data: %{id: 1}
        })

      with_mock HTTPoison, janus_mock(:ok, body, 200) do
        assert [%{"room" => 1}] == XbinsWeb.Janus.list_rooms()
      end
    end

    test "400 response" do
      body = Poison.encode!(%{})

      with_mock HTTPoison, janus_mock(:ok, body, 400) do
        assert is_nil(XbinsWeb.Janus.list_rooms())
      end
    end

    test "error response" do
      body = Poison.encode!(%{})

      with_mock HTTPoison, janus_mock(:error, body, 500) do
        assert is_nil(XbinsWeb.Janus.list_rooms())
      end
    end
  end

  defp janus_mock(status, body, code) do
    response =
      if status == :ok do
        {status, %HTTPoison.Response{body: body, status_code: code}}
      else
        {status, %HTTPoison.Error{reason: nil}}
      end

    [
      post: fn _url, _body -> response end,
      start: fn -> nil end
    ]
  end
end
