defmodule XbinsWeb.PowTestHelpers do
  def authenticate(conn, user) do
    conn
    |> Pow.Plug.put_config(current_user_assigns_key: :current_user)
    |> Pow.Plug.assign_current_user(user, otp_app: :xbins_web)
  end
end
