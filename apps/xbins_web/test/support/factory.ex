# https://hexdocs.pm/ecto/test-factories.html
defmodule Xbins.Factory do
  alias Db.Repo

  # Factories

  def build(:learner_package_quiz) do
    %Db.LearnerPackage.LearnerPackageQuiz{}
  end

  def build(:user) do
    %Db.Users.User{
      first_name: "TestFirstName",
      last_name: "TestLastName",
      type: :candidate,
      email: "test-email-#{System.unique_integer()}@local-test.com"
    }
  end

  # Convenience API

  def build(factory_name, attributes) do
    factory_name |> build() |> struct!(attributes)
  end

  def insert!(factory_name, attributes \\ []) do
    factory_name |> build(attributes) |> Repo.insert!()
  end
end
