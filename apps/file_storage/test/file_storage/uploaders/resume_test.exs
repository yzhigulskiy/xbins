defmodule FileStorage.Uploaders.ResumeTest do
  use ExUnit.Case
  alias FileStorage.Uploaders.Resume

  test "store file" do
    current_user = %{id: 1}
    assert Resume.store({"test/fixtures/sample.pdf", current_user}) == {:ok, "sample.pdf"}
  end

  test "validates file extension" do
    current_user = %{id: 1}
    assert Resume.store({"test/fixtures/sample.jpg", current_user}) == {:error, :invalid_file}
  end
end
