defmodule FileStorage.Uploaders.Resume do
  use Waffle.Definition
  use Waffle.Ecto.Definition

  @versions [:original]
  @extension_whitelist ~w(.pdf .doc .docx)

  def validate({file, _}) do
    file_extension = file.file_name |> Path.extname |> String.downcase
    Enum.member?(@extension_whitelist, file_extension)
  end

  def filename(version, {_file, scope}) do
    "#{scope.id}-#{version}"
  end

  def storage_dir(version, _opts) do
    "uploads/resume/"
  end
end
