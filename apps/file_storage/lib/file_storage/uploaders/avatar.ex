defmodule FileStorage.Uploaders.Avatar do
  use Waffle.Definition
  use Waffle.Ecto.Definition

  @versions [:original]

  def validate({file, _}) do
    ~w(.jpg .jpeg .gif .png) |> Enum.member?(Path.extname(file.file_name))
  end

  def filename(version, {_file, scope}) do
    "#{scope.id}-#{version}"
  end

  def storage_dir(version, _opts) do
    "uploads/avatars/"
  end
end
