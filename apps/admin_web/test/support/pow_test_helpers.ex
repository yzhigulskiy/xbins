defmodule AdminWeb.PowTestHelpers do
  def authenticate(conn, user) do
    conn
    |> Pow.Plug.put_config(current_user_assigns_key: :current_user)
    |> Pow.Plug.assign_current_user(user, otp_app: :admin_web)
  end
end
