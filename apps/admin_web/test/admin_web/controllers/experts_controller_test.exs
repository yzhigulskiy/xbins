defmodule AdminWeb.ExpertsControllerTest do
  use AdminWeb.ConnCase

  setup %{conn: conn} do
    admin = %Db.Admins.Admin{}
    auth_conn = conn |> authenticate(admin)

    expert1 =
      insert(
        :expert_user,
        first_name: "Bob",
        last_name: "Tee",
        linkedin_profile_url: "http://cool/expert",
        education: "University and major courses",
        experience: "Lorem ipsum dolor 2"
      )

    expert2 = insert(:expert_user, first_name: "Bi", last_name: "Tri")
    insert(:candidate_user)

    {
      :ok,
      conn: auth_conn, expert1: expert1, expert2: expert2
    }
  end

  describe "GET /experts" do
    test "should show 2 experts", %{conn: conn} do
      conn = get(conn, Routes.experts_path(conn, :index))

      assert response = html_response(conn, 200)

      response
      |> assert_html("table.admin-table")
      |> assert_html("tbody tr", count: 2)
    end

    test "check contents of first row", %{conn: conn, expert1: expert} do
      conn = get(conn, Routes.experts_path(conn, :index))

      assert response = html_response(conn, 200)

      assert_html(response, "table.admin-table tbody tr:first-child", fn row ->
        row
        |> assert_html("td:nth-child(1)", "#{expert.id}")
        |> assert_html("td:nth-child(2)", "Bob Tee")
        |> assert_html("td:nth-child(3)", "<i class=\"icon icon-file\">")
        |> assert_html("td:nth-child(4)", "<i class=\"icon icon-linkedin\">")
        |> assert_html("td:nth-child(5)", expert.email)
        |> assert_html("td:nth-child(6)", "Scheduled")
        |> assert_html("td:nth-child(7)", "Awaiting")
        |> assert_html("td:nth-child(8)", "5.0")
        |> assert_html("td:nth-child(9)", "<i class=\"icon icon-verified\">")
        |> assert_html("td:nth-child(10) a",
          href: Routes.experts_path(conn, :show, expert.id)
        )
      end)
    end
  end

  describe "GET /experts/:id" do
    test "back arrow link", %{conn: conn, expert1: expert} do
      conn = get(conn, Routes.experts_path(conn, :show, expert.id))

      assert response = html_response(conn, 200)

      assert_html(
        response,
        ".users-page a.content-back-button",
        href: Routes.experts_path(conn, :index)
      )
    end

    test "expert with all fields filled", %{conn: conn, expert1: expert} do
      conn = get(conn, Routes.experts_path(conn, :show, expert.id))

      assert response = html_response(conn, 200)

      assert_html(response, ".users-page .content-container", fn html ->
        assert html =~ "#{expert.id}"
        assert html =~ expert.first_name
        assert html =~ expert.last_name
        assert html =~ expert.email
        assert html =~ expert.linkedin_profile_url

        html
        |> assert_html("p.education", expert.education)
        |> assert_html("p.experience", expert.experience)
        |> assert_html(".row-action-buttons", fn action_row ->
          action_row
          |> assert_html(".col-message-button a", "Message")
          |> assert_html(".col-delete-button a", "Delete This Account")
        end)
      end)
    end

    test "expert without optional fields", %{conn: conn, expert2: expert} do
      conn = get(conn, Routes.experts_path(conn, :show, expert.id))

      assert response = html_response(conn, 200)

      assert_html(response, ".users-page .content-container", fn html ->
        assert html =~ "#{expert.id}"
        assert html =~ expert.first_name
        assert html =~ expert.last_name
        assert html =~ expert.email

        html
        |> assert_html("p.linkedin-link", "Remind to add")
        |> assert_html("p.education", "Remind to add")
        |> assert_html("p.experience", "Remind to add")
        |> assert_html(".row-action-buttons", fn action_row ->
          action_row
          |> assert_html(".col-message-button a", "Message")
          |> assert_html(".col-delete-button a", "Delete This Account")
        end)
      end)
    end
  end
end
