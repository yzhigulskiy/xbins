defmodule AdminWeb.PaymentsControllerTest do
  use AdminWeb.ConnCase

  setup %{conn: conn} do
    admin = %Db.Admins.Admin{email: "test@test.com"}
    conn = authenticate(conn, admin)
    pending_payment = insert(:pending_payment)
    success_payment = insert(:success_payment)
    failed_payment = insert(:failed_payment)

    {
      :ok,
      conn: conn,
      pending_payment: pending_payment,
      success_payment: success_payment,
      failed_payment: failed_payment
    }
  end

  describe "GET /payments" do
    test "returns list of payments", %{
      conn: conn,
      pending_payment: pending_payment,
      success_payment: success_payment
    } do
      conn = get(conn, Routes.payments_path(conn, :index))

      assert response = html_response(conn, 200)
      assert response =~ pending_payment.card_name_external
      assert response =~ success_payment.card_name_external
      assert response =~ success_payment.sale_id_external
    end
  end

  describe "GET /payments/:id" do
    test "show success payment", %{conn: conn, success_payment: payment} do
      conn = get(conn, Routes.payments_path(conn, :show, payment.id))

      assert response = html_response(conn, 200)
      assert response =~ "#{payment.user_id}"
      assert response =~ payment.card_name_external
      assert response =~ payment.sale_id_external
      assert response =~ "Approved"
      refute response =~ "Message to Candidate"
      assert response =~ "$#{payment.amount}"
    end

    test "show failed payment", %{conn: conn, failed_payment: payment} do
      conn = get(conn, Routes.payments_path(conn, :show, payment.id))

      assert response = html_response(conn, 200)
      assert response =~ "#{payment.user_id}"
      assert response =~ payment.card_name_external
      assert response =~ payment.sale_id_external
      assert response =~ "Denied"
      assert response =~ "Message to Candidate"
      assert response =~ "$#{payment.amount}"
    end
  end
end
