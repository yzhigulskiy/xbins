defmodule AdminWeb.DashboardControllerTest do
  use AdminWeb.ConnCase

  setup %{conn: conn} do
    admin = %Db.Admins.Admin{email: "test@test.com"}
    {:ok, conn: conn, admin: admin}
  end

  describe "GET /" do
    test "with unauthenticated admin", %{conn: conn} do
      conn = get(conn, "/")
      assert redirected_to(conn) == Routes.pow_session_path(conn, :new, request_path: "/")
    end

    test "with authenticated admin", %{conn: conn, admin: user} do
      conn = conn |> authenticate(user) |> get("/")
      assert html_response(conn, 200) =~ "Admin"
    end
  end
end
