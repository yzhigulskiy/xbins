defmodule AdminWeb.CandidatesControllerTest do
  use AdminWeb.ConnCase

  setup %{conn: conn} do
    admin = %Db.Admins.Admin{}
    auth_conn = conn |> authenticate(admin)

    candidate1 =
      insert(
        :candidate_user,
        first_name: "Tod",
        last_name: "Lee",
        linkedin_profile_url: "test",
        education: "University and courses",
        experience: "Lorem ipsum dolor"
      )

    candidate2 = insert(:candidate_user, first_name: "Bob", last_name: "Bee")
    insert(:expert_user)

    {
      :ok,
      conn: auth_conn, candidate1: candidate1, candidate2: candidate2
    }
  end

  describe "GET /candidates" do
    test "should show 2 candidates", %{conn: conn} do
      conn = get(conn, Routes.candidates_path(conn, :index))

      assert response = html_response(conn, 200)

      response
      |> assert_html("table.admin-table")
      |> assert_html("tbody tr", count: 2)
    end

    test "check contents of first row", %{conn: conn, candidate1: candidate} do
      conn = get(conn, Routes.candidates_path(conn, :index))

      assert response = html_response(conn, 200)

      assert_html(response, "table.admin-table tbody tr:first-child", fn row ->
        row
        |> assert_html("td:nth-child(1)", "#{candidate.id}")
        |> assert_html("td:nth-child(2)", "Tod Lee")
        |> assert_html("td:nth-child(3)", "<i class=\"icon icon-file\">")
        |> assert_html("td:nth-child(4)", "<i class=\"icon icon-linkedin\">")
        |> assert_html("td:nth-child(5)", candidate.email)
        |> assert_html("td:nth-child(6)", "Active")
        |> assert_html("td:nth-child(7)", "Awaiting")
        |> assert_html("td:nth-child(8)", "5.0")
        |> assert_html("td:nth-child(9) a",
          href: Routes.candidates_path(conn, :show, candidate.id)
        )
      end)
    end
  end

  describe "GET /candidates/:id" do
    test "back arrow link", %{conn: conn, candidate1: candidate} do
      conn = get(conn, Routes.candidates_path(conn, :show, candidate.id))

      assert response = html_response(conn, 200)

      assert_html(
        response,
        ".users-page a.content-back-button",
        href: Routes.candidates_path(conn, :index)
      )
    end

    test "candidate with all fields filled", %{conn: conn, candidate1: candidate} do
      conn = get(conn, Routes.candidates_path(conn, :show, candidate.id))

      assert response = html_response(conn, 200)

      assert_html(response, ".users-page .content-container", fn html ->
        assert html =~ "#{candidate.id}"
        assert html =~ candidate.first_name
        assert html =~ candidate.last_name
        assert html =~ candidate.email
        assert html =~ candidate.linkedin_profile_url

        html
        |> assert_html("p.education", candidate.education)
        |> assert_html("p.experience", candidate.experience)
        |> assert_html(".row-action-buttons", fn action_row ->
          action_row
          |> assert_html(".col-message-button a", "Message")
          |> assert_html(".col-delete-button a", "Delete This Account")
        end)
      end)
    end

    test "candidate without optional fields", %{conn: conn, candidate2: candidate} do
      conn = get(conn, Routes.candidates_path(conn, :show, candidate.id))

      assert response = html_response(conn, 200)

      assert_html(response, ".users-page .content-container", fn html ->
        assert html =~ "#{candidate.id}"
        assert html =~ candidate.first_name
        assert html =~ candidate.last_name
        assert html =~ candidate.email

        html
        |> assert_html("p.linkedin-link", "Remind to add")
        |> assert_html("p.education", "Remind to add")
        |> assert_html("p.experience", "Remind to add")
        |> assert_html(".row-action-buttons", fn action_row ->
          action_row
          |> assert_html(".col-message-button a", "Message")
          |> assert_html(".col-delete-button a", "Delete This Account")
        end)
      end)
    end
  end
end
