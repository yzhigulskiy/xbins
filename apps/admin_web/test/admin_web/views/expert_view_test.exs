defmodule AdminWeb.ExpertViewTest do
  use AdminWeb.ConnCase, async: true

  alias AdminWeb.ExpertsView

  test "verified_icon_class/1" do
    assert ExpertsView.verified_icon_class(true) == "icon-verified"
    assert ExpertsView.verified_icon_class(false) == "icon-unverified"
  end
end
