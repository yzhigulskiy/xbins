defmodule AdminWeb.SharedHelpersTest do
  use AdminWeb.ConnCase, async: true

  alias AdminWeb.SharedHelpers

  test "full_name/1" do
    user = build(:user, first_name: "Lord", last_name: "Dark")
    assert SharedHelpers.full_name(user) == "Lord Dark"
  end

  test "user_status_badge_class/1" do
    assert SharedHelpers.user_status_badge_class(:completed) == "badge-status-completed"
    assert SharedHelpers.user_status_badge_class(:active) == "badge-status-active"
  end

  test "user_status_badge_name/1" do
    assert SharedHelpers.user_status_badge_name(:completed) == "Completed"
    assert SharedHelpers.user_status_badge_name(:active) == "Active"
  end

  describe "rating_progress/1" do
    test "when rating is 1.1" do
      html = 1.1 |> SharedHelpers.rating_progress() |> Phoenix.HTML.safe_to_string()

      assert html ==
               ~s(<i class="icon icon-rating"></i><i class="icon icon-rating-empty">) <>
                 ~s(</i><i class="icon icon-rating-empty"></i><i class="icon icon-rating-empty"></i>) <>
                 ~s(<i class="icon icon-rating-empty"></i>1.1)
    end

    test "when rating is 4.6" do
      html = 4.6 |> SharedHelpers.rating_progress() |> Phoenix.HTML.safe_to_string()

      assert html ==
               ~s(<i class="icon icon-rating"></i><i class="icon icon-rating">) <>
                 ~s(</i><i class="icon icon-rating"></i><i class="icon icon-rating"></i>) <>
                 ~s(<i class="icon icon-rating"></i>4.6)
    end
  end
end
