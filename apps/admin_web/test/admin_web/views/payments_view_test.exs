defmodule AdminWeb.PaymentsViewTest do
  use AdminWeb.ConnCase, async: true

  alias AdminWeb.PaymentsView

  test "formatted_payment_time/1" do
    datetime = ~N[2020-08-11 20:51:51]
    assert PaymentsView.formatted_payment_time(datetime) == "11 Aug 2020, 08:51 PM"
  end

  test "status_badge_class/1" do
    assert PaymentsView.status_badge_class(:success) == "badge-payment-success"
    assert PaymentsView.status_badge_class(:fail) == "badge-payment-fail"
  end

  test "status_badge_text/1" do
    assert PaymentsView.status_badge_text(:pending) == "In progress"
    assert PaymentsView.status_badge_text(:success) == "Approved"
    assert PaymentsView.status_badge_text(:fail) == "Denied"
  end
end
