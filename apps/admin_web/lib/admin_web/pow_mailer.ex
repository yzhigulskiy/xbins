defmodule AdminWeb.PowMailer do
  use Pow.Phoenix.Mailer
  use Bamboo.Mailer, otp_app: :admin_web

  import Bamboo.Email

  def cast(%{user: user, subject: subject, text: text, html: html}) do
    new_email(
      to: user.email,
      from: default_from(),
      subject: subject,
      text_body: text,
      html_body: html
    )
  end

  def process(email) do
    deliver_now(email)
  end

  defp default_from do
    Application.get_env(:admin_web, :mailer)[:from]
  end
end
