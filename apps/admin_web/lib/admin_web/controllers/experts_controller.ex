defmodule AdminWeb.ExpertsController do
  use AdminWeb, :controller

  def index(conn, _params) do
    users = Db.User.list_experts()
    render(conn, :index, users: users)
  end

  def show(conn, %{"id" => id}) do
    user = Db.User.get_expert(id)
    render(conn, :show, user: user)
  end
end
