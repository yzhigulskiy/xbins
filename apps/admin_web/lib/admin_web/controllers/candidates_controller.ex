defmodule AdminWeb.CandidatesController do
  use AdminWeb, :controller

  def index(conn, _params) do
    users = Db.User.list_candidates()
    render(conn, :index, users: users)
  end

  def show(conn, %{"id" => id}) do
    user = Db.User.get_candidate(id)
    render(conn, :show, user: user)
  end
end
