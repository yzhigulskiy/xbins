defmodule AdminWeb.PaymentsController do
  use AdminWeb, :controller
  import Ecto.Query, only: [order_by: 2]

  alias Db.Payments.Payment

  def index(conn, _params) do
    payments = Payment |> order_by({:desc, :id}) |> Db.Repo.all()
    render(conn, :index, payments: payments)
  end

  def show(conn, %{"id" => id}) do
    payment = Db.Payment.get(id)
    render(conn, :show, payment: payment)
  end
end
