defmodule AdminWeb.Pow.Routes do
  use Pow.Phoenix.Routes
  alias AdminWeb.Router.Helpers, as: Routes

  def after_reset_password_path(conn), do: Routes.dashboard_path(conn, :index)
end
