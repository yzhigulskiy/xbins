defmodule AdminWeb.LayoutView do
  use AdminWeb, :view

  def dashboard_page?(conn) do
    Routes.dashboard_path(conn, :index) == Phoenix.Controller.current_path(conn)
  end

  def candidates_page?(conn) do
    conn
    |> Phoenix.Controller.current_path()
    |> String.starts_with?(Routes.candidates_path(conn, :index))
  end

  def experts_page?(conn) do
    conn
    |> Phoenix.Controller.current_path()
    |> String.starts_with?(Routes.experts_path(conn, :index))
  end

  def payments_page?(conn) do
    conn
    |> Phoenix.Controller.current_path()
    |> String.starts_with?(Routes.payments_path(conn, :index))
  end
end
