defmodule AdminWeb.PowResetPassword.MailerView do
  use AdminWeb, :mailer_view

  def subject(:reset_password, _assigns), do: "Reset password link"
end
