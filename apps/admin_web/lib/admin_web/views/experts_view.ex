defmodule AdminWeb.ExpertsView do
  use AdminWeb, :view

  def verified_icon_class(verified) do
    case verified do
      true -> "icon-verified"
      _ -> "icon-unverified"
    end
  end
end
