defmodule AdminWeb.SharedHelpers do
  use Phoenix.HTML
  import Phoenix.HTML.Tag

  def full_name(user) do
    "#{user.first_name} #{user.last_name}"
  end

  def user_status_badge_name(status) do
    Phoenix.Naming.humanize(status)
  end

  def user_status_badge_class(status) do
    "badge-status-#{status}"
  end

  def rating_progress(value) do
    stars =
      Enum.map(1..5, fn i ->
        class = if i > round(value), do: "icon-rating-empty", else: "icon-rating"
        content_tag(:i, "", class: "icon #{class}")
      end)

    ~e[<%= stars %><%= value %>]
  end
end
