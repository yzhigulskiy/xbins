defmodule AdminWeb.PaymentsView do
  use AdminWeb, :view

  def formatted_payment_time(datetime) do
    Timex.format!(datetime, "%d %b %Y, %I:%M %p", :strftime)
  end

  def status_badge_class(status) do
    "badge-payment-#{status}"
  end

  def status_badge_text(status) do
    case status do
      :pending -> "In progress"
      :success -> "Approved"
      :fail -> "Denied"
    end
  end
end
