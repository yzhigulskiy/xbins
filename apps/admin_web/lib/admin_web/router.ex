defmodule AdminWeb.Router do
  use AdminWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :protected do
    plug Pow.Plug.RequireAuthenticated,
      error_handler: Pow.Phoenix.PlugErrorHandler
  end

  pipeline :session_layout do
    plug :put_layout, {AdminWeb.LayoutView, "session.html"}
  end

  scope "/" do
    pipe_through [:browser, :session_layout]

    pow_session_routes()
    pow_extension_routes()

    get "/reset_password/note", AdminWeb.ResetPasswordController, :note
    post "/reset_password", AdminWeb.ResetPasswordController, :create
  end

  scope "/", AdminWeb do
    pipe_through [:browser, :protected]

    get "/", DashboardController, :index
    resources "/candidates", CandidatesController, only: [:index, :show]
    resources "/experts", ExpertsController, only: [:index, :show]
    resources "/payments", PaymentsController, only: [:index, :show]
  end

  if Mix.env() == :dev do
    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end

  # Other scopes may use custom stacks.
  # scope "/api", AdminWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: AdminWeb.Telemetry
    end
  end
end
