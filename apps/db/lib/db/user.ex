defmodule Db.User do
  import Ecto.Query, warn: false
  alias Db.Repo

  alias Db.Users.User, as: Entry

  def get_user(id), do: Repo.get!(Entry, id)

  def list_candidates do
    Entry
    |> where([u], u.type == ^:candidate)
    |> order_by({:asc, :id})
    |> Repo.all()
  end

  def list_experts do
    Entry
    |> where([u], u.type == ^:expert)
    |> order_by({:asc, :id})
    |> Repo.all()
  end

  def get_candidate(id) do
    Entry |> Repo.get_by!(id: id, type: :candidate)
  end

  def get_expert(id) do
    Entry |> Repo.get_by!(id: id, type: :expert)
  end

  def update_candidate(user = %Entry{type: :candidate}, attrs) do
    user
    |> Entry.candidate_changeset(attrs)
    |> Repo.update()
  end

  def update_avatar(user = %Entry{}, attrs) do
    user
    |> Entry.avatar_changeset(attrs)
    |> Repo.update()
  end

  def change_candidate(user = %Entry{type: :candidate}, attrs \\ %{}) do
    Entry.candidate_changeset(user, attrs)
  end
end
