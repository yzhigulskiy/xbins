defmodule Db.LearnerPackage do
  @moduledoc """
  The LearnerPackage context.
  """

  import Ecto.Query, warn: false
  alias Db.Repo

  alias Db.LearnerPackage.LearnerPackageQuiz

  @doc """
  Returns the list of learner_package_quizzes.

  ## Examples

      iex> list_learner_package_quizzes()
      [%LearnerPackageQuiz{}, ...]

  """
  def list_learner_package_quizzes do
    Repo.all(LearnerPackageQuiz)
  end

  @doc """
  Gets a single learner_package_quiz.

  Raises `Ecto.NoResultsError` if the Learner package quiz does not exist.

  ## Examples

      iex> get_learner_package_quiz!(123)
      %LearnerPackageQuiz{}

      iex> get_learner_package_quiz!(456)
      ** (Ecto.NoResultsError)

  """
  def get_learner_package_quiz!(id), do: Repo.get!(LearnerPackageQuiz, id)

  def find_or_create_quiz!(token) do
    quiz = LearnerPackageQuiz |> where(token: ^token) |> Repo.one()

    if quiz do
      quiz
    else
      {:ok, quiz} = Repo.insert(%LearnerPackageQuiz{token: token})
      quiz
    end
  end

  @doc """
  Creates a learner_package_quiz.

  ## Examples

      iex> create_learner_package_quiz(%{field: value})
      {:ok, %LearnerPackageQuiz{}}

      iex> create_learner_package_quiz(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_learner_package_quiz(attrs \\ %{}) do
    %LearnerPackageQuiz{}
    |> LearnerPackageQuiz.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a learner_package_quiz.

  ## Examples

      iex> update_learner_package_quiz(learner_package_quiz, %{field: new_value})
      {:ok, %LearnerPackageQuiz{}}

      iex> update_learner_package_quiz(learner_package_quiz, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_learner_package_quiz(learner_package_quiz = %LearnerPackageQuiz{}, attrs) do
    learner_package_quiz
    |> LearnerPackageQuiz.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a learner_package_quiz.

  ## Examples

      iex> delete_learner_package_quiz(learner_package_quiz)
      {:ok, %LearnerPackageQuiz{}}

      iex> delete_learner_package_quiz(learner_package_quiz)
      {:error, %Ecto.Changeset{}}

  """
  def delete_learner_package_quiz(learner_package_quiz = %LearnerPackageQuiz{}) do
    Repo.delete(learner_package_quiz)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking learner_package_quiz changes.

  ## Examples

      iex> change_learner_package_quiz(learner_package_quiz)
      %Ecto.Changeset{data: %LearnerPackageQuiz{}}

  """
  def change_learner_package_quiz(learner_package_quiz = %LearnerPackageQuiz{}, attrs \\ %{}) do
    LearnerPackageQuiz.changeset(learner_package_quiz, attrs)
  end

  def step_name_for(learner_package_quiz) do
    if learner_package_quiz.step_name do
      next_step(learner_package_quiz.step_name)
    else
      hd(LearnerPackageQuiz.known_steps())
    end
  end

  def next_step(step_name) do
    if index = Enum.find_index(LearnerPackageQuiz.known_steps(), &(&1 == step_name)) do
      Enum.at(LearnerPackageQuiz.known_steps(), index + 1)
    end
  end
end
