defmodule Db.LearnerPackage.LearnerPackageQuiz do
  use Ecto.Schema
  import Ecto.Changeset

  schema "learner_package_quizzes" do
    field :token, :string
    field :step_name, :string, default: "name"
    field :first_name, :string
    field :email, :string
    field :are_you_actively_interviewing, :boolean

    field :topics_elevator_pitch, :boolean
    field :topics_behavioral_interview, :boolean
    field :topics_unstructured_interview, :boolean
    field :topics_stress_interview, :boolean
    field :topics_im_not_sure, :boolean

    field :communication_channel, :string

    field :need_help_negotiating_salary, :boolean

    timestamps()
  end

  @known_steps [
    "name",
    "email",
    "interviewing",
    "interviewing-keepgoing",
    "topics",
    "topics-keepgoing",
    "communication",
    "communication-selected",
    "salary",
    "form-result"
  ]

  def changeset(learner_package_quiz, attrs = %{"step_name" => "name"}) do
    learner_package_quiz
    |> cast(attrs, [:step_name, :first_name])
    |> validate_required([:step_name, :first_name])
  end

  def changeset(learner_package_quiz, attrs = %{"step_name" => "email"}) do
    learner_package_quiz
    |> cast(attrs, [:step_name, :email])
    |> validate_required([:step_name, :email])
  end

  def changeset(learner_package_quiz, attrs = %{"step_name" => "interviewing"}) do
    learner_package_quiz
    |> cast(attrs, [:step_name, :are_you_actively_interviewing])
    |> validate_required([:step_name, :are_you_actively_interviewing])
  end

  def changeset(learner_package_quiz, attrs = %{"step_name" => "interviewing-keepgoing"}) do
    learner_package_quiz
    |> cast(attrs, [:step_name])
    |> validate_required([:step_name])
  end

  def changeset(learner_package_quiz, attrs = %{"step_name" => "topics"}) do
    learner_package_quiz
    |> cast(attrs, [
      :step_name,
      :topics_elevator_pitch,
      :topics_behavioral_interview,
      :topics_unstructured_interview,
      :topics_stress_interview,
      :topics_im_not_sure
    ])
    |> validate_required([
      :step_name,
      :topics_elevator_pitch,
      :topics_behavioral_interview,
      :topics_unstructured_interview,
      :topics_stress_interview,
      :topics_im_not_sure
    ])
  end

  def changeset(learner_package_quiz, attrs = %{"step_name" => "topics-keepgoing"}) do
    learner_package_quiz
    |> cast(attrs, [:step_name])
    |> validate_required([:step_name])
  end

  def changeset(learner_package_quiz, attrs = %{"step_name" => "communication"}) do
    learner_package_quiz
    |> cast(attrs, [:step_name, :communication_channel])
    |> validate_required([:step_name, :communication_channel])
  end

  def changeset(learner_package_quiz, attrs = %{"step_name" => "communication-selected"}) do
    learner_package_quiz
    |> cast(attrs, [:step_name])
    |> validate_required([:step_name])
  end

  def changeset(learner_package_quiz, attrs = %{"step_name" => "salary"}) do
    learner_package_quiz
    |> cast(attrs, [:step_name, :need_help_negotiating_salary])
    |> validate_required([:step_name, :need_help_negotiating_salary])
  end

  def changeset(learner_package_quiz, attrs = %{"token" => _token}) do
    learner_package_quiz
    |> cast(attrs, [:token, :step_name])
    |> validate_required([:token])
  end

  def changeset(learner_package_quiz, attrs) do
    learner_package_quiz
    |> cast(attrs, [:step_name])
    |> validate_required([:step_name])
  end

  def known_steps do
    @known_steps
  end
end
