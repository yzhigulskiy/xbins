defmodule Db.Users.User do
  use Ecto.Schema
  use Pow.Ecto.Schema
  use Waffle.Ecto.Schema

  use Pow.Extension.Ecto.Schema,
    extensions: [PowResetPassword]

  import Ecto.Changeset

  import Pow.Ecto.Schema.Changeset,
    only: [new_password_changeset: 3, current_password_changeset: 3]

  schema "users" do
    pow_user_fields()
    field :first_name, :string
    field :last_name, :string
    field :expert_field, :string
    field :terms_of_service, :boolean, virtual: true
    field :resume, FileStorage.Uploaders.Resume.Type
    field :avatar, FileStorage.Uploaders.Avatar.Type
    field :linkedin_profile_url, :string
    field :education, :string
    field :experience, :string
    field :type, Db.EctoEnums.UserTypeEnum, default: :candidate

    timestamps()
  end

  def changeset(user_or_changeset, attrs) do
    user_or_changeset
    |> pow_changeset(attrs)
    |> pow_extension_changeset(attrs)
    |> cast(attrs, [
      :first_name,
      :last_name,
      :expert_field,
      :terms_of_service,
      :type
    ])
    |> validate_required([
      :first_name,
      :last_name,
      :expert_field,
      :email,
      :password_hash,
      :type
    ])
    |> validate_acceptance(:terms_of_service)
  end

  def avatar_changeset(user_or_changeset, attrs) do
    user_or_changeset
    |> cast_attachments(attrs, [:avatar])
    |> validate_required([:avatar])
  end

  def candidate_changeset(user_or_changeset, attrs) do
    user_or_changeset
    |> cast_attachments(attrs, [:resume])
    |> cast(attrs, [
      :first_name,
      :last_name,
      :linkedin_profile_url,
      :education,
      :experience,
      :type
    ])
    |> maybe_change_user_password(attrs)
    |> validate_required([:first_name, :last_name, :type])
  end

  def maybe_change_user_password(user_or_changeset, attrs) do
    if attrs[:current_password] do
      user_or_changeset
      |> current_password_changeset(attrs, @pow_config)
      |> new_password_changeset(attrs, @pow_config)
    else
      user_or_changeset
    end
  end
end
