defmodule Db.Payments.Payment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "payments" do
    belongs_to :user, Db.Users.User

    field :status, Db.EctoEnums.PaymentStatusEnum
    field :amount, :decimal
    field :card_name_external, :string
    field :reference_id_external, :string
    field :sale_id_external, :string
    field :success_called_at, :utc_datetime
    field :postback_called_at, :utc_datetime

    timestamps()
  end

  def changeset(payment, attrs = %{status: :pending}) do
    payment
    |> cast(attrs, [
      :user_id,
      :status,
      :amount,
      :reference_id_external,
      :sale_id_external,
      :card_name_external
    ])
    |> validate_required([
      :user_id,
      :status,
      :reference_id_external,
      :amount
    ])
  end

  def changeset(payment, attrs) do
    payment
    |> cast(attrs, [
      :user_id,
      :amount,
      :status,
      :card_name_external,
      :reference_id_external,
      :sale_id_external,
      :success_called_at,
      :postback_called_at
    ])
    |> validate_required([
      :user_id,
      :amount,
      :status,
      :card_name_external,
      :reference_id_external,
      :sale_id_external,
      :success_called_at,
      :postback_called_at
    ])
  end
end
