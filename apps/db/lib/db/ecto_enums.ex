defmodule Db.EctoEnums do
  import EctoEnum

  defenum(UserTypeEnum, ["candidate", "expert"])
  defenum(PaymentStatusEnum, ["success", "fail", "pending"])
end
