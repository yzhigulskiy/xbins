defmodule Db.Payment do
  alias Db.Payments.Payment
  alias Db.Repo

  def get(payment_id) do
    Repo.get!(Payment, payment_id)
  end

  def create(attrs) do
    %Payment{}
    |> Payment.changeset(Map.put(attrs, :status, :pending))
    |> Repo.insert()
  end
end
