defmodule Db.LearnerPackageTest do
  use Db.DataCase

  alias Db.LearnerPackage

  describe "learner_package_quizzes" do
    alias Db.LearnerPackage.LearnerPackageQuiz

    @valid_attrs %{
      "token" => "#{System.unique_integer()}-db-test"
    }
    @update_attrs %{
      "step_name" => "email",
      "email" => "some updated email"
    }
    @invalid_attrs %{"step_name" => "email"}

    def learner_package_quiz_fixture(attrs \\ %{}) do
      {:ok, learner_package_quiz} =
        attrs
        |> Enum.into(@valid_attrs)
        |> LearnerPackage.create_learner_package_quiz()

      learner_package_quiz
    end

    test "list_learner_package_quizzes/0 returns all learner_package_quizzes" do
      learner_package_quiz = learner_package_quiz_fixture()
      assert LearnerPackage.list_learner_package_quizzes() == [learner_package_quiz]
    end

    test "get_learner_package_quiz!/1 returns the learner_package_quiz with given id" do
      learner_package_quiz = learner_package_quiz_fixture()

      assert LearnerPackage.get_learner_package_quiz!(learner_package_quiz.id) ==
               learner_package_quiz
    end

    test "create_learner_package_quiz/1 with valid data creates a learner_package_quiz" do
      assert {:ok, %LearnerPackageQuiz{} = learner_package_quiz} =
               LearnerPackage.create_learner_package_quiz(@valid_attrs)

      assert learner_package_quiz.token
    end

    test "create_learner_package_quiz/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               LearnerPackage.create_learner_package_quiz(@invalid_attrs)
    end

    test "update_learner_package_quiz/2 with valid data updates the learner_package_quiz" do
      learner_package_quiz = learner_package_quiz_fixture()

      assert {:ok, %LearnerPackageQuiz{} = learner_package_quiz} =
               LearnerPackage.update_learner_package_quiz(learner_package_quiz, @update_attrs)

      assert learner_package_quiz.email == "some updated email"
    end

    test "update_learner_package_quiz/2 with invalid data returns error changeset" do
      learner_package_quiz = learner_package_quiz_fixture()

      assert {:error, %Ecto.Changeset{}} =
               LearnerPackage.update_learner_package_quiz(learner_package_quiz, @invalid_attrs)

      assert learner_package_quiz ==
               LearnerPackage.get_learner_package_quiz!(learner_package_quiz.id)
    end

    test "delete_learner_package_quiz/1 deletes the learner_package_quiz" do
      learner_package_quiz = learner_package_quiz_fixture()

      assert {:ok, %LearnerPackageQuiz{}} =
               LearnerPackage.delete_learner_package_quiz(learner_package_quiz)

      assert_raise Ecto.NoResultsError, fn ->
        LearnerPackage.get_learner_package_quiz!(learner_package_quiz.id)
      end
    end

    test "change_learner_package_quiz/1 returns a learner_package_quiz changeset" do
      learner_package_quiz = learner_package_quiz_fixture()
      assert %Ecto.Changeset{} = LearnerPackage.change_learner_package_quiz(learner_package_quiz)
    end
  end
end
