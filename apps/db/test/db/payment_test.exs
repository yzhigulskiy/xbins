defmodule Db.PaymentTest do
  use Db.DataCase, async: true

  alias Db.Payment

  setup do
    user = insert(:user)
    payment = insert(:pending_payment, user: user)

    %{user: user, payment: payment}
  end

  describe "get/1" do
    test "return payment", %{payment: payment} do
      assert Payment.get(payment.id).id == payment.id
    end
  end

  describe "create/1" do
    test "with valid data creates payment", %{user: user} do
      valid_attrs = %{
        user_id: user.id,
        amount: 100.0,
        reference_id_external: "12345"
      }

      assert {:ok, %Db.Payments.Payment{} = payment} = Payment.create(valid_attrs)
    end

    test "with invalid data returns error changeset" do
      invalid_attrs = %{}

      assert {:error, %Ecto.Changeset{}} = Payment.create(invalid_attrs)
    end
  end
end
