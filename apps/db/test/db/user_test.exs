defmodule Db.UserTest do
  use Db.DataCase, async: true

  alias Db.User

  describe "list_candidates/0" do
    setup do
      insert(:expert_user)
      :ok
    end

    test "two candidates exists" do
      users = insert_list(2, :candidate_user)
      assert User.list_candidates() == users
    end

    test "no candidates exists" do
      assert User.list_candidates() == []
    end
  end

  describe "list_expertes/0" do
    setup do
      insert(:candidate_user)
      :ok
    end

    test "two experts exists" do
      users = insert_list(2, :expert_user)
      assert User.list_experts() == users
    end

    test "no experts exists" do
      assert User.list_experts() == []
    end
  end

  describe "get_candidate/1" do
    setup do
      expert = insert(:expert_user)
      candidate = insert(:candidate_user)
      {:ok, candidate: candidate, expert: expert}
    end

    test "when candidate exists", %{candidate: candidate} do
      assert User.get_candidate(candidate.id)
    end

    test "when candidate is not found", %{expert: expert} do
      assert_raise Ecto.NoResultsError, fn ->
        User.get_candidate(expert.id)
      end
    end
  end

  describe "get_expert/1" do
    setup do
      expert = insert(:expert_user)
      candidate = insert(:candidate_user)
      {:ok, candidate: candidate, expert: expert}
    end

    test "when expert exists", %{expert: expert} do
      assert User.get_expert(expert.id)
    end

    test "when expert is not found", %{candidate: candidate} do
      assert_raise Ecto.NoResultsError, fn ->
        User.get_expert(candidate.id)
      end
    end
  end
end
