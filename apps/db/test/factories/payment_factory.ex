defmodule Db.PaymentFactory do
  defmacro __using__(_opts) do
    quote do
      def pending_payment_factory(attrs) do
        reference_id_external = sequence(:reference_id_external, &"ref-#{&1}")
        card_name_external = sequence(:card_name_external, &"XXXXXXXXXXXX68#{&1}")

        payment = %Db.Payments.Payment{
          user: build(:user),
          status: :pending,
          amount: 300.0,
          reference_id_external: reference_id_external,
          card_name_external: card_name_external
        }

        merge_attributes(payment, attrs)
      end

      def success_payment_factory(attrs) do
        attrs
        |> pending_payment_factory()
        |> merge_attributes(%{status: :success, sale_id_external: "ex-sale-12345"})
      end

      def failed_payment_factory(attrs) do
        attrs
        |> pending_payment_factory()
        |> merge_attributes(%{status: :fail, sale_id_external: "ex-sale-12345"})
      end
    end
  end
end
