defmodule Db.UserFactory do
  alias Pow.Ecto.Schema.Password

  defmacro __using__(_opts) do
    quote do
      def user_factory(attrs) do
        password = Map.get(attrs, :password, "1234567890")
        encrypted_password = Password.pbkdf2_hash(password)

        user = %Db.Users.User{
          email: sequence(:email, &"email-#{&1}@example.com"),
          first_name: "Bonnie",
          last_name: "Clyde",
          type: :candidate
        }

        user
        |> merge_attributes(attrs)
        |> merge_attributes(%{password_hash: encrypted_password})
      end

      def candidate_user_factory(attrs) do
        attrs
        |> user_factory()
        |> merge_attributes(%{type: :candidate})
      end

      def expert_user_factory(attrs) do
        attrs
        |> user_factory()
        |> merge_attributes(%{type: :expert})
      end
    end
  end
end
