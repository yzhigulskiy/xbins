defmodule Db.Factory do
  use ExMachina.Ecto, repo: Db.Repo

  use Db.UserFactory
  use Db.PaymentFactory
end
