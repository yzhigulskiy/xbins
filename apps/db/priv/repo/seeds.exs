# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Db.Repo.insert!(%Db.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

%Db.Admins.Admin{}
|> Db.Admins.Admin.pow_changeset(%{
  email: "admin@xbins.test",
  password: "admin@xbins.test",
  confirm_password: "admin@xbins.test"
})
|> Db.Repo.insert!()
