defmodule Db.Repo.Migrations.AddUserType do
  use Ecto.Migration
  import Ecto.Query, only: [from: 1]

  alias Db.Users.User
  alias Db.Repo

  def up do
    alter table(:users) do
      add :type, :string, null: true
    end

    flush()
    Repo.update_all(from(a in User), set: [type: "candidate"])

    alter table(:users) do
      modify :type, :string, null: false
    end
  end

  def down do
    alter table(:admins) do
      remove :type
    end
  end
end
