defmodule XbinsWeb.Repo.Migrations.CreateLearnerPackageQuizzes do
  use Ecto.Migration

  def change do
    create table(:learner_package_quizzes) do
      add :token, :string, null: false
      add :step_name, :string
      add :first_name, :string
      add :email, :string
      add :are_you_actively_interviewing, :boolean

      add :topics_elevator_pitch, :boolean
      add :topics_behavioral_interview, :boolean
      add :topics_unstructured_interview, :boolean
      add :topics_stress_interview, :boolean
      add :topics_im_not_sure, :boolean

      add :communication_channel, :string

      add :need_help_negotiating_salary, :boolean

      timestamps()
    end
  end
end
