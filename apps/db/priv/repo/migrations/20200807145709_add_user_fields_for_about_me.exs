defmodule Db.Repo.Migrations.AddUserFieldsForAboutMe do
  use Ecto.Migration

  def change do
    alter table("users") do
      add :linkedin_profile_url, :string
      add :resume, :string
      add :education, :string
      add :experience, :text
    end
  end
end
