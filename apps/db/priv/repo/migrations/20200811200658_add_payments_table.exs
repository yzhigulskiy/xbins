defmodule Db.Repo.Migrations.AddPaymentsTable do
  use Ecto.Migration

  def change do
    create table(:payments) do
      add :user_id, :integer
      add :status, :string
      add :amount, :decimal, precision: 10, scale: 2
      add :card_name_external, :string
      add :reference_id_external, :string
      add :sale_id_external, :string
      add :success_called_at, :utc_datetime
      add :postback_called_at, :utc_datetime

      timestamps()
    end
  end
end
