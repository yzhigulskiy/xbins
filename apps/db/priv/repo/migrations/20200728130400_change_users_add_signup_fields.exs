defmodule Db.Repo.Migrations.ChangeUsersAddSignupFields do
  use Ecto.Migration

  def change do
    alter table("users") do
      add :first_name, :string
      add :last_name, :string
      add :expert_field, :string
    end
  end
end
