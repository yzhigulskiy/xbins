# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.

use Mix.Config

# Configure Mix tasks and generators
config :db,
  ecto_repos: [Db.Repo]

config :admin_web,
  ecto_repos: [Db.Repo]

config :admin_web, :pow,
  user: Db.Admins.Admin,
  repo: Db.Repo,
  web_module: AdminWeb,
  web_mailer_module: AdminWeb,
  extensions: [PowResetPassword],
  controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks,
  routes_backend: AdminWeb.Pow.Routes,
  mailer_backend: AdminWeb.PowMailer

config :admin_web, AdminWeb.PowMailer, adapter: Bamboo.LocalAdapter

config :admin_web, :mailer, from: {"Better Interviews", "info@xbins.test"}

config :xbins_web, ecto_repos: [Db.Repo]

config :xbins_web, :pow,
  user: Db.Users.User,
  repo: Db.Repo,
  web_module: XbinsWeb,
  extensions: [PowResetPassword],
  routes_backend: XbinsWeb.Pow.Routes,
  controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks

# Configures the endpoint
config :admin_web, AdminWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "0KqbyXEOsOqsN4pbl6ppHV1l5g2ONDg0pXlAWxfO5DvnAl8jyHejV0LrUbl7Srkb",
  render_errors: [view: AdminWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: AdminWeb.PubSub,
  live_view: [signing_salt: "3F1QRi+7"]

# Configures the endpoint
config :xbins_web, XbinsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "228lLYcyk2XAtqyBUnZ+zNb1eUn/9uCGAy3KdwloMMPv/mDSu+z4gIBCt061M8mf",
  render_errors: [view: XbinsWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: XbinsWeb.PubSub,
  live_view: [signing_salt: "wvz0bcam"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :waffle,
  storage: Waffle.Storage.Local

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
