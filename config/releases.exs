import Config

database_url =
  System.get_env("DATABASE_URL") ||
    raise """
    environment variable DATABASE_URL is missing.
    For example: ecto://USER:PASS@HOST/DATABASE
    """

config :db, Db.Repo,
  ssl: false,
  url: database_url,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

admin_host = System.get_env("ADMIN_HOST") || raise "environment variable ADMIN_HOST is missing."

config :admin_web, AdminWeb.Endpoint,
  server: true,
  url: [host: admin_host],
  http: [
    port: String.to_integer(System.get_env("XBINS_WEB_PORT") || "4000"),
    transport_options: [socket_opts: [:inet6]]
  ],
  secret_key_base: secret_key_base

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
#     config :admin_web, AdminWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.

host = System.get_env("HOST") || raise "environment variable HOST is missing."

config :xbins_web, XbinsWeb.Endpoint,
  server: true,
  url: [host: host],
  http: [
    port: String.to_integer(System.get_env("ADMIN_WEB_PORT") || "4001"),
    transport_options: [socket_opts: [:inet6]]
  ],
  secret_key_base: secret_key_base

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
#     config :xbins_web, XbinsWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.
