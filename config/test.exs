use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :admin_web, AdminWeb.Endpoint,
  http: [port: 4003],
  server: false

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :xbins_web, XbinsWeb.Endpoint,
  http: [port: 4002],
  server: false

config :logger, level: :warn

config :admin_web, AdminWeb.PowMailer, adapter: Bamboo.TestAdapter
config :pow, Pow.Ecto.Schema.Password, iterations: 1

config :waffle,
  storage: Waffle.Storage.Local,
  storage_dir_prefix: Path.expand(Path.join(__DIR__, "../apps/file_storage"))

config :goth, disabled: true

import_config "test.secret.exs"
